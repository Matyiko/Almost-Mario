using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kameramo : MonoBehaviour
{
    public GameObject target;
    public GameObject cam;
    public Vector3 offset;

    


    // Start is called before the first frame update
    void Start()
    {
        cam.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        cam.transform.position = target.transform.position+offset;
    }
}
