using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LifeScript : MonoBehaviour
{

    public Image[] lives;
    public int maxLife;
    private int livesRemaining;

    public void LoseLife(){
        livesRemaining--;
        lives[livesRemaining].enabled = false;
        if(livesRemaining == 0){
            SceneManager.LoadScene(0);
        }
    }

    public void ResetLives(){
        livesRemaining = maxLife;
        lives[0].enabled = true;
        lives[1].enabled = true;
        lives[2].enabled = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        livesRemaining = maxLife;
       // ResetLives();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
