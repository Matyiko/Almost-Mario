using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VilagGeneralas : MonoBehaviour
{
    /*Generalo generalo;
    [SerializeField] int maxszelesseg;
    [SerializeField] int maxmagassag;*/

    [SerializeField] GameObject jatekos;
    [SerializeField] int iteracio;


    [SerializeField] GameObject lebego;
    [SerializeField] GameObject fold;
    [SerializeField] GameObject lathatatlanFal;
    [SerializeField] GameObject var;
    [SerializeField] GameObject zaszlo;
    [SerializeField] GameObject erme;
    [SerializeField] GameObject ellenseg0;
    [SerializeField] GameObject ellenseg1;
    /*[SerializeField] GameObject ellenseg2;
    [SerializeField] GameObject ellenseg3;*/
    int szint = 0;


    public void Start()
    {
        szint++;
        if (szint == 1)
        {
            Generalo.valtozok = new Valtozok(
                blokkValtozokFeltoltese(),
                hatterelemValtozokFeltoltese(),
                interakcioValtozokFeltoltese(),
                ellensegValtozokFeltoltese()
            );
        }
        Generalo.valtozok.kezdopontBeallit(((int)jatekos.GetComponent<Transform>().position.x));


        (new Kezdopont(
            iteralas: iteracio //!!kezdésnek nem a valószínűségét, hanem az iterálható formák mennyiségét adjuk meg
        )).Generalas();
    }

    void Update()
    {

    }

    private Dictionary<string, GameObject> blokkValtozokFeltoltese()
    {
        Dictionary<string, GameObject> valtozok = new Dictionary<string, GameObject>();
        valtozok["lebego"] = lebego;
        valtozok["fold"] = fold;
        valtozok["lathatatlanfal"] = lathatatlanFal;

        return valtozok;

    }

    private Dictionary<string, GameObject> hatterelemValtozokFeltoltese()
    {
        Dictionary<string, GameObject> valtozok = new Dictionary<string, GameObject>();
        valtozok["var"] = var;

        return valtozok;

    }

    private Dictionary<string, GameObject> interakcioValtozokFeltoltese()
    {
        Dictionary<string, GameObject> valtozok = new Dictionary<string, GameObject>();
        valtozok["zaszlo"] = zaszlo;
        valtozok["erme"] = erme;

        return valtozok;

    }

    private Dictionary<string, GameObject> ellensegValtozokFeltoltese()
    {
        Dictionary<string, GameObject> valtozok = new Dictionary<string, GameObject>();
        valtozok["ellenseg0"] = ellenseg0;
        valtozok["ellenseg1"] = ellenseg1;
        /*valtozok["ellenseg2"] = ellenseg2;
        valtozok["ellenseg3"] = ellenseg3;*/

        return valtozok;

    }

    public static void Megjelenites(Object obj, Vector2 koordinata, Quaternion quaternion, bool lehet_rajta_coin = true, bool lehet_rajta_ellenseg=true)
    {
        Instantiate(obj, koordinata, quaternion);
        randomMegjelenites(koordinata, quaternion, lehet_rajta_coin, lehet_rajta_ellenseg);
    }

    private static void randomMegjelenites(Vector2 koordinata, Quaternion quaternion, bool lehet_rajta_coin, bool lehet_rajta_ellenseg){
        int esely = Random.Range(0, 18);
        if (esely<2)
            {
                if(lehet_rajta_coin){
                    coinMegjelenites(koordinata, quaternion);
                }
            }
         else if(esely==2){
             if(lehet_rajta_ellenseg){
                ellensegMegjelenites(koordinata, quaternion);
             }
         }
    }

    private static void coinMegjelenites(Vector2 koordinata, Quaternion quaternion){        
        var coin_koordinata = new Vector2(koordinata.x, koordinata.y + 1);
        Instantiate(Generalo.valtozok.interakciok["erme"], coin_koordinata, quaternion);
        
    }

    private static void ellensegMegjelenites(Vector2 koordinata, Quaternion quaternion){
        var ellenseg_koordinata = new Vector2(koordinata.x, koordinata.y + 1);
        var ellensegszam = Random.Range(0, Generalo.valtozok.ellensegek.Count);
        Instantiate(Generalo.valtozok.ellensegek.Values.ElementAt(ellensegszam), ellenseg_koordinata, quaternion);
    }

}

//Blokkok külön változóban tárolása, hogy minden Generáló osztály el tudja érni
public class Valtozok
{
    public Dictionary<string, GameObject> blokkok;
    public Dictionary<string, GameObject> hatterelemek;
    public Dictionary<string, GameObject> interakciok;
    public Dictionary<string, GameObject> ellensegek;
    public int MINIMUM_MAGASSAG = -5; //ennél lejjebb nem lehet feltölteni a pályát blokkal
    public int MAXIMUM_MAGASSAG=5; // ennél magasabbra nem generálhat égi blokk
    public int MINIMUM_SZELESSEG;
    public int kezdopont;

    public Valtozok(
        Dictionary<string, GameObject> blokkok,
        Dictionary<string, GameObject> hatterelemek,
        Dictionary<string, GameObject> interakciok,
        Dictionary<string, GameObject> ellensegek)
    {
    
        this.blokkok = blokkok;
        this.hatterelemek = hatterelemek;
        this.interakciok = interakciok;
        this.ellensegek=ellensegek;

        Generalo.magassag = MINIMUM_MAGASSAG + 1;
       
    }

    public void kezdopontBeallit(int kezdopont){
        this.kezdopont = kezdopont;
        MINIMUM_SZELESSEG = this.kezdopont - 10;
         Generalo.generalas_folytatas = MINIMUM_SZELESSEG;
    }
}

public class EgiBlokkGeneralas
{
    public int utolsoGeneraltVizszintes = -10; //az utolsó generált égben lévő blokk x koordinátája, kezdetben egy kellően alacsony szám
    public EgiBlokkAllapot allapot = new BiztosanNemGeneralEgiBlokk(1);

    public void NemGeneralhato()
    {
        allapot = new BiztosanNemGeneralEgiBlokk(allapot.biztosanGeneralasKovetkezikValoszinuseg);
    }

    public void Generalas(int magassag, int szelesseg)
    {
        if(Generalo.valtozok.MAXIMUM_MAGASSAG>magassag){
            if (allapot.Generalas(magassag, szelesseg))
            {
                utolsoGeneraltVizszintes = szelesseg;
            }
            allapot = allapot.KovetkezoAllapot();
        }
    }
}

public abstract class EgiBlokkAllapot
{
    public int biztosanGeneralasKovetkezikValoszinuseg; // 0-10-es skálán ennyi a valószínűsége, hogy blokk követi
    public int magassag;

    public EgiBlokkAllapot(int valoszinuseg)
    {
        this.biztosanGeneralasKovetkezikValoszinuseg = valoszinuseg;
    }
    public abstract bool Generalas(int magassag, int szelesseg); //ha generált blokkot akkor igazzal tér vissza
    public abstract EgiBlokkAllapot KovetkezoAllapot();
}
public class BiztosanNemGeneralEgiBlokk : EgiBlokkAllapot
{

    public BiztosanNemGeneralEgiBlokk(int valoszinuseg) : base(valoszinuseg)
    {
    }

    public override bool Generalas(int magassag, int selesseg)
    {
        return false;
    }
    public override EgiBlokkAllapot KovetkezoAllapot()
    {
        var random_esely = Random.Range(0, 10);
        if (random_esely < biztosanGeneralasKovetkezikValoszinuseg)
        {
            return new BiztosanGeneralEgiBlokk(5);
        }
        biztosanGeneralasKovetkezikValoszinuseg++;
        return this;
    }
}

public class BiztosanGeneralEgiBlokk : EgiBlokkAllapot
{
    int fixgeneralas = 2;
    public BiztosanGeneralEgiBlokk(int valoszinuseg) : base(valoszinuseg)
    {
    }
    public override bool Generalas(int magassag, int szelesseg)
    {
        VilagGeneralas.Megjelenites(Generalo.valtozok.blokkok["lebego"], new Vector2(szelesseg, magassag), Quaternion.identity);
        return true;
    }
    public override EgiBlokkAllapot KovetkezoAllapot()
    {
        if (fixgeneralas > 0)
        {
            fixgeneralas--;
            return this;
        }

        var random_esely = Random.Range(0, 10);
        if (random_esely < biztosanGeneralasKovetkezikValoszinuseg)
        {
            biztosanGeneralasKovetkezikValoszinuseg--;
            return this;
        }
        return new BiztosanNemGeneralEgiBlokk(1);
    }
}

public abstract class Generalo
{
    public static EgiBlokkGeneralas egiblokkGeneralas = new EgiBlokkGeneralas();
    public static Valtozok valtozok = null; //megjeleníthető blokkok
    public int valoszinuseg; //adott forma köbetkezésének valószínűsége
    protected List<Generalo> generalhato; //a formát követhető formák gyűjteménye a következő iterációban
    protected static int MAX_ITERACIO; // inicializálás a kezdésben & összesen generálható formák száma
    protected static int iteracio = 0; //ennyi forma generálás történt eddig

    public static int magassag; //ezen a magasságon kezdődik a pálya generálása
    public static int generalas_folytatas; //ennél a vízszintes pontnál tart a generálás

    public Generalo(int valoszinuseg)
    {
        this.valoszinuseg = valoszinuseg;
    }

    public abstract void ValoszinusegFeltolt();

    public void UjFajtaGeneralas()
    {
        int MAXVALOSZINUSEG = 0;

        //őt követő formák valószínűségénk összege
        generalhato.ForEach(it =>
        {
            MAXVALOSZINUSEG += it.valoszinuseg;
        });

        var valasztas = Random.Range(0, MAXVALOSZINUSEG);
        int i = 0;
        //kivetítjük egy síkra a valószínűségeket
        //pl ha Sik(3), Luk(2) ebben a sorrendben van a gyűjteményben
        //akkor egy 5 hosszú szakaszban 0,1,2-es érték 
        //a Sík valószínűsége, 3,4-es a Luk valószínűsége

        foreach (var ujobjektum in generalhato)
        {
            i += ujobjektum.valoszinuseg;

            if (valasztas <= i)
            {
                ujobjektum.ValoszinusegFeltolt();
                ujobjektum.Generalas();
                break;
            }
        }
    }

    public void Iteralas()
    {
        iteracio++;
        if (iteracio <= MAX_ITERACIO)
        {
            //új forma generálása a következő iterációban
            UjFajtaGeneralas();
        }
        else
        {
            (new Vegpont()).Generalas();
            iteracio = 0;
        }
    }

    public abstract void Generalas();
    protected int Szelesseg(int max_szelesseg = 10, int min_szelesseg = 1)
    {
        return Random.Range(min_szelesseg, max_szelesseg);
    }


    //föld "belsejének" generálása
    public void Kitoltes()
    {
        for (int i = magassag - 1; i >= valtozok.MINIMUM_MAGASSAG; i--)
        {
            VilagGeneralas.Megjelenites(valtozok.blokkok["fold"], new Vector2(generalas_folytatas, i), Quaternion.identity, lehet_rajta_coin: false, lehet_rajta_ellenseg:false);
        }
    }

}

public abstract class varGeneralo : Generalo
{
    private int generalashossz = valtozok.kezdopont - valtozok.MINIMUM_SZELESSEG; //generalas_folytatas negatívtól indul
    public varGeneralo(int valoszinuseg) : base(valoszinuseg)
    {

    }

    public override void Generalas()
    {
        felSik();
        varGeneralas();
        felSik();
    }

    protected void varGeneralas()
    {
        VilagGeneralas.Megjelenites(valtozok.hatterelemek["var"], new Vector2(generalas_folytatas, magassag + 4), Quaternion.identity, lehet_rajta_coin: false, lehet_rajta_ellenseg:false);
    }

    protected void felSik()
    {
        for (int i = 0; i < generalashossz / 2; i++)
        {
            VilagGeneralas.Megjelenites(valtozok.blokkok["fold"], new Vector2(generalas_folytatas, magassag), Quaternion.identity, lehet_rajta_coin: false, lehet_rajta_ellenseg: false);
            Kitoltes();
            generalas_folytatas++;
        }
    }

    protected void lathatatlanFal(int eltolas)
    { // paraméter: mennyivel később generálódjon a generalas_folytatashoz képest (hogy a játékos ne ragadjon be elé/mögé)
        for (int i = magassag + 1; i < magassag + 10; i++)
        {
            VilagGeneralas.Megjelenites(valtozok.blokkok["lathatatlanfal"], new Vector2(generalas_folytatas + eltolas, i), Quaternion.identity, lehet_rajta_coin: false, lehet_rajta_ellenseg:false);
        }
    }
}

public class Kezdopont : varGeneralo
{
    public Kezdopont(int iteralas) : base(0)
    { //a valószínűsége lényegtelen
        MAX_ITERACIO = iteralas;
        ValoszinusegFeltolt();
    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        generalhato.Add(new SikKitoltott(1));
        generalhato.Add(new Luk(5));
        generalhato.Add(new Lepcsofel(3));
        generalhato.Add(new Emelkedes(2));
        //generalhato.Add(new Szakadek(1));
    }

    public override void Generalas()
    {
        base.Generalas();
        lathatatlanFal(-2);
        felSik();
        Iteralas();
    }
}

public class Vegpont : varGeneralo
{
    public Vegpont() : base(0)
    {

    }

    public override void ValoszinusegFeltolt()
    {

    }

    public override void Generalas()
    {
        zaszloGeneralas();
        lathatatlanFal(+2);
        base.Generalas();
    }

    private void zaszloGeneralas()
    {
        VilagGeneralas.Megjelenites(valtozok.interakciok["zaszlo"], new Vector2(generalas_folytatas + 0.7f, magassag + 1.3f), Quaternion.identity, lehet_rajta_coin: false, lehet_rajta_ellenseg:false);
    }
}

public class SikKitoltetlen : Sik
{

    public SikKitoltetlen(int valoszinuseg) : base(valoszinuseg)
    {
    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        generalhato.Add(new Luk(5));
    }

    private int Emelkedes()
    {
        return Random.Range(0, 2);
    }

    public override GameObject Tipus(){
        return valtozok.blokkok["lebego"];
    }

    public override void Generalas()
    {
        int emelkedes = Emelkedes();

        if (emelkedes != 0)
        {
            magassag += Emelkedes();
            egiblokkGeneralas.NemGeneralhato();
        }
        base.Generalas();
    }
}

public abstract class Sik : Generalo
{
    public Sik(int valoszinuseg) : base(valoszinuseg)
    {
    }

    virtual public void kitoltesEngedve()
    {
        //nincs engedve
    }

    abstract public GameObject Tipus();

    public override void Generalas()
    {
        int szelesseg = Szelesseg();
        for (int i = 0; i < szelesseg; i++)
        {
            VilagGeneralas.Megjelenites(Tipus(), new Vector2(generalas_folytatas, magassag), Quaternion.identity);
            kitoltesEngedve();

            egiblokkGeneralas.Generalas(Generalo.magassag + 3, generalas_folytatas/*, i==szelesseg-1*/);
            //RandomEgiBlokk(magassag);
            generalas_folytatas++;
        }
        Iteralas();
    }


}

public class SikKitoltott : Sik
{
    public SikKitoltott(int valoszinuseg) : base(valoszinuseg)
    {
    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        if (magassag == valtozok.MINIMUM_MAGASSAG + 1)
        {
            generalhato.Add(new Lepcsofel(3));
            generalhato.Add(new Emelkedes(5));
        }
        else
        {
            generalhato.Add(new Szakadek(2));
            generalhato.Add(new Lepcsole(6));
        }
        generalhato.Add(new Luk(3));
        //generalhato.Add(new Lepcsofel(5));
        //generalhato.Add(new Lepcsole(5));
        //generalhato.Add(new Luk(3));

    }

    public override GameObject Tipus()
    {
        return valtozok.blokkok["fold"];
    }

    public override void kitoltesEngedve()
    {
        Kitoltes();
    }
}

public class Lepcsofel : Generalo
{
    public Lepcsofel(int valoszinuseg) : base(valoszinuseg)
    {
    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        generalhato.Add(new SikKitoltott(4));
        generalhato.Add(new Lepcsole(5));
        generalhato.Add(new Luk(8));

    }

    public override void Generalas()
    {
        egiblokkGeneralas.NemGeneralhato();
        for (int i = 0; i < Szelesseg(max_szelesseg: 6); i++)
        {
            VilagGeneralas.Megjelenites(valtozok.blokkok["fold"], new Vector2(generalas_folytatas, magassag), Quaternion.identity, lehet_rajta_ellenseg: false);
            Kitoltes();
            magassag++;
            generalas_folytatas++;
        }
        Iteralas();

    }


}

public class Lepcsole : Generalo
{
    public Lepcsole(int valoszinuseg) : base(valoszinuseg)
    {
    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        generalhato.Add(new SikKitoltott(6));
        //generalhato.Add(new Lepcsofel(5));
        //generalhato.Add(new Lepcsole(5));
        generalhato.Add(new Luk(3));

    }

    public override void Generalas()
    {
        int i = 0; //ennyi lesz majd a tényleges szélesség
        int kezdoSzelesseg = generalas_folytatas;
        int kezdetiMagassag = magassag;

        for (; i < Szelesseg(); i++)
        {
            if (magassag == valtozok.MINIMUM_MAGASSAG + 1)
            {
                break;
            }
            magassag--;
            VilagGeneralas.Megjelenites(valtozok.blokkok["fold"], new Vector2(generalas_folytatas, magassag), Quaternion.identity, lehet_rajta_ellenseg: false);
            Kitoltes();
            generalas_folytatas++;
        }
        egiBlokkGeneralas(kezdoSzelesseg, i, kezdetiMagassag + 3);
        egiblokkGeneralas.NemGeneralhato();
        Iteralas();
    }

    private void egiBlokkGeneralas(int kezdoSzelesseg, int szelesseg, int fixmagassag)
    {
        for (int i = 0 + kezdoSzelesseg; i < szelesseg + kezdoSzelesseg; i++)
        {
            Generalo.egiblokkGeneralas.Generalas(magassag: fixmagassag, szelesseg: i);
        }
    }
}

public class Luk : Generalo
{
    public Luk(int valoszinuseg) : base(valoszinuseg)
    {
    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        generalhato.Add(new SikKitoltott(7));
        generalhato.Add(new SikKitoltetlen(5));
        if (magassag == valtozok.MINIMUM_MAGASSAG + 1)
        {
            generalhato.Add(new Lepcsofel(1));
            generalhato.Add(new Emelkedes(4));
        }
        else
        {
            generalhato.Add(new Szakadek(7));
            generalhato.Add(new Lepcsole(10));
        }

    }

    /*private void egiBlokkGeneralas(int szelesseg, int fixmagassag){
        var kezdo_szelesseg = Random.Range(1,4);
        egiblokkGeneralas.Generalas(magassag: fixmagassag, szelesseg:kezdo_szelesseg, biztosanGeneral: true);

    }*/

    private int egiBlokkMagassag()
    {
        //return Random.Range(magassag-3, magassag+3);
        return magassag + 3;
    }

    private bool megeloziEgiblokk()
    {
        return (Generalo.generalas_folytatas - 3 <= Generalo.egiblokkGeneralas.utolsoGeneraltVizszintes);
    }

    public override void Generalas()
    {
        int szelesseg = Szelesseg(
            max_szelesseg: 4,
            min_szelesseg: 2);

        var egyszegesEgiBlokkMagassag = egiBlokkMagassag();

        for (int i = generalas_folytatas; i < generalas_folytatas + szelesseg; i++)
        {
            if(megeloziEgiblokk()){
                egiblokkGeneralas.Generalas(magassag: egyszegesEgiBlokkMagassag, szelesseg: i);
            }
        }

        generalas_folytatas += szelesseg;
        Iteralas();
    }
}

public class Szakadek : Generalo
{
    public Szakadek(int valoszinuseg) : base(valoszinuseg)
    {
    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        generalhato.Add(new SikKitoltott(6));
        generalhato.Add(new Luk(3));
    }

    public override void Generalas()
    {
        egiblokkGeneralas.NemGeneralhato();
        magassag = valtozok.MINIMUM_MAGASSAG + 1;
        Iteralas();
    }
}

public class Emelkedes : Generalo
{
    public Emelkedes(int valoszinuseg) : base(valoszinuseg)
    {

    }

    public override void ValoszinusegFeltolt()
    {
        generalhato = new List<Generalo>();
        generalhato.Add(new SikKitoltott(4));
        generalhato.Add(new Lepcsole(5));
        //generalhato.Add(new Luk(3));
    }

    private bool megeloziEgiblokk()
    {
        return (Generalo.generalas_folytatas - 3 <= Generalo.egiblokkGeneralas.utolsoGeneraltVizszintes);
    }

    private int Magassag()
    {
        int maximum = 2;
        if (megeloziEgiblokk())
        {
            maximum = 6;
        }

        return Random.Range(2, maximum);
    }

    private void miniSik()
    {
        VilagGeneralas.Megjelenites(valtozok.blokkok["fold"], new Vector2(generalas_folytatas, magassag), Quaternion.identity);
        Kitoltes();
        generalas_folytatas++;
    }

    public override void Generalas()
    {
        miniSik();
        magassag += Magassag();
        miniSik();
        Iteralas();
    }
}


