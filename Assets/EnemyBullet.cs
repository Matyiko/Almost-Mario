using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float dieTime;
    public GameObject diePEFFECT;
    public Movement player;
    public LayerMask playerLayer;
    public Collider2D bodyCollider;
    public int damage=1;

    void Start()
    {
        StartCoroutine(CountDownTimer());
    }

    void OnColisionEnter2D(Collision2D col)
    {
        Die();
    }

    IEnumerator CountDownTimer()
    {
        yield return new WaitForSeconds(dieTime);

        Die();
    }

    void OnTriggerEnter2D(Collider2D collision){
        //if(collision.gameObject.tag == "Player"){
          //  player.TakeDamage(damage);
            Die();
        //}
    }

    void Update()
    {
        /*if (bodyCollider.IsTouchingLayers(playerLayer))
        {
            player.TakeDamage(damage);
            Die();
        }*/
    }

    void Die()
    {
        Destroy(gameObject);
    }

}
