using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Movement : MonoBehaviour
{

    public LayerMask groundLayer;

    private Transform trans;
    private Animator anim;
    private Rigidbody2D rb2D;

    private Vector2 levelPos;

    private float moveSpeed;
    private float jumpForce;
    private float moveVertical;
    private float moveHorizotal;
    private bool isUntarget;
    private bool isDead;
    private bool canGenerate;

    public int maxHealth = 3;
    private int currentHealth;

    public float timeRemainingUntarget = 0;
    public float timeRemainingRestart = 0;

    public float timeRemainingGenerateMap = 0;

    public HealthBarScript healthBar;
    public VilagGeneralas vilagGeneralas;
    public LifeScript lifeScript;
    // Start is called before the first frame update
    void Start()
    {
        trans = gameObject.GetComponent<Transform>();
        anim = gameObject.GetComponent<Animator>();
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        moveSpeed = 3f;
        jumpForce = 50f;
        isUntarget = false;
        isDead = false;
        canGenerate = true;
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        levelPos = new Vector2(trans.position.x,trans.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        moveHorizotal = Input.GetAxisRaw("Horizontal");
        moveVertical = Input.GetAxisRaw("Vertical");
        if (timeRemainingUntarget > 0)
        {
            timeRemainingUntarget -= Time.deltaTime;
        }else{ 
            isUntarget = false;
        }
        if (timeRemainingRestart > 0)
        {
            timeRemainingRestart -= Time.deltaTime;
        }else{ 
            if(isDead){
                currentHealth = maxHealth;
                healthBar.SetHealth(currentHealth);
                anim.SetTrigger("respawn");
                trans.position = levelPos;
            }
            isDead = false;
        }
        if (timeRemainingGenerateMap > 0)
        {
            timeRemainingGenerateMap -= Time.deltaTime;
        }else{ 
            canGenerate = true;
        }
        if((trans.position.y < -4.59) && (!isDead)){
            currentHealth = 0;
            healthBar.SetHealth(currentHealth);
            Die();
        }
    }

    void FixedUpdate(){
        if(!isDead){
            if(moveHorizotal == 0.0f){           
                anim.SetBool("isWalking", false);
            }
            if(moveHorizotal > 0.1f || moveHorizotal < -0.1f){                      
                anim.SetBool("isWalking", true);
                if(moveHorizotal > 0.1f){
                    Vector3 scale = trans.localScale;
                    scale.x = 1;
                    trans.localScale = scale;
                }else{
                    Vector3 scale = trans.localScale;
                    scale.x = -1;
                    trans.localScale = scale;
                }
                if(!isGrounded()){
                    rb2D.AddForce(new Vector2(moveHorizotal * moveSpeed * 0.5f, 0.0f), ForceMode2D.Impulse);
                }else{
                    rb2D.AddForce(new Vector2(moveHorizotal * moveSpeed, 0.0f), ForceMode2D.Impulse);
                }
                
            }
            if((moveVertical > 0.1f) && isGrounded()){
                rb2D.AddForce(new Vector2(0f,moveVertical * jumpForce),ForceMode2D.Impulse);
                anim.SetBool("isJumping", true);
            }
            if(!isGrounded()){
                anim.SetBool("isJumping", true);
            }else{
                anim.SetBool("isJumping", false);
            }       
        }
    }
    
    void OnTriggerEnter2D(Collider2D collision){
        if(collision.gameObject.tag == "Enemy"){
            if(isUntarget == false){
                timeRemainingUntarget = 1;
                isUntarget = true;
                rb2D.AddForce(new Vector2(0f,0.5f * jumpForce),ForceMode2D.Impulse);
                TakeDamage(1);
            }
            
        }
        if(collision.gameObject.tag == "Finish"){
            if(canGenerate){
                timeRemainingGenerateMap = 1;
                trans.position=new Vector2(trans.position.x+25,trans.position.y); 
                levelPos = new Vector2(trans.position.x,trans.position.y);
                vilagGeneralas.Start();
                canGenerate = false;
            }            
        }
    }

    public void TakeDamage(int damage){
        currentHealth -= damage;
        if(currentHealth == 0){
            Die();
        }
        healthBar.SetHealth(currentHealth);
    }

    void Die(){
        anim.SetTrigger("isDead");
        isDead = true;
        timeRemainingRestart = 2;
        lifeScript.LoseLife();
    }

    bool isGrounded(){
        var oldal1 = new Vector2(transform.position.x + 0.28f, transform.position.y);
        var oldal2 = new Vector2(transform.position.x - 0.28f, transform.position.y);
        RaycastHit2D hit1 = Physics2D.Raycast(oldal1, Vector2.down, 1.5f / 2, groundLayer);
        RaycastHit2D hit2 = Physics2D.Raycast(oldal2, Vector2.down, 1.5f / 2, groundLayer);
        if((hit1.collider != null) || (hit2.collider != null)){
            return true;
        }else{
            return false;
        }
    }
    
}
