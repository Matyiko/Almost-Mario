using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIJump : MonoBehaviour
{
    public float jumpSpeed;

    [HideInInspector]
    private bool mustJump;
    // Start is called before the first frame update

    public Rigidbody2D rb;
    public LayerMask groundLayer;
    public Collider2D bodyCollider;
    public float bounceForce;

    void Start()
    {
        mustJump = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        while (mustJump)
        {
            Jump();
        }
    }

    void Jump()
    {
        if (bodyCollider.IsTouchingLayers(groundLayer))
        {
            rb.velocity = new Vector2(jumpSpeed * Time.fixedDeltaTime, rb.velocity.y);
        }
    }
}
