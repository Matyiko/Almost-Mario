using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

public class Coin : MonoBehaviour
{
    private float coin = 0;

    public TextMeshProUGUI textCoins;
    private bool destroyed = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Coin" && destroyed == false)
        {
            //destroyed = true;
            Destroy(other.gameObject);
            coin++;
            textCoins.text = coin.ToString();
            StartCoroutine(ToggleBool());

            string path = "Assets/Resources/text.txt";
            StreamReader reader = new StreamReader(path);
            float hasonlitas = float.Parse(reader.ReadToEnd());
            reader.Close();
            if (hasonlitas < coin) {
            StreamWriter writer = new StreamWriter(path, false);
                writer.WriteLine(coin.ToString());
                writer.Close();
            }
            

            
        }
    }

    IEnumerator ToggleBool() {
        destroyed = true;
        yield return new WaitForSeconds(0.05f);
        destroyed = false;
    }

}
