using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cammove : MonoBehaviour
{
    [SerializeField] private GameObject robot;

    private void Start()
    {
        // As fallback get it only ONCE
        if (!robot) robot = GameObject.Find("Player");
    }

    void Update()
    {
        // get the current position
        var position = transform.position;
        // overwrite only the X component
        position.x = robot.transform.position.x;
        // assign the new position back
        transform.position = position;
    }
}
