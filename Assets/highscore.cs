using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

public class highscore : MonoBehaviour
{
    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        string path = "Assets/Resources/text.txt";
        StreamReader reader = new StreamReader(path);
        text.text = reader.ReadToEnd();
        reader.Close();
        
    }

   
}
