# Almost Mario

## A group University project created in Unity which is like a simple version of the Original Mario

The structure of the game:

* A main character that you can move by the WASD keys and space to jump.
* A random generated maps that are doable.
* Different enemies that can kill you.
* Parallax background.

My part of the project:

* I created the three different enemies :

    * Stationary
    * Patrolling
    * Stationary but Shooting to both sides

* I created the parallax background. I painted 4-6 different layers for the background that were moving on different speed creating a the parallax effect. It was very intresting because that was the first time i met that kind of design.

## Environment
    Unity 2D

## Language
    C#

## Version
    1.0.15


